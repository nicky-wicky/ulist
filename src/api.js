/*  jshint esversion: 6 */

import { listen, emit } from './dispatcher';
import actions from './actions';
import $ from 'jquery';
import toast from './toast';

var ko = '/html/ulist';
var bd = 'tel';

listen(actions.API_GET_ALL, () => {
  $.ajax({
    method: 'GET',
    url: ko + '/api.php/' + bd + '?transform=1'
  })
    .done(function (msg) {
      if (msg) {
        //  console.log(msg)
        emit(actions.API_GET_ALL_SUCCESS, msg[bd]);
      }
      emit(actions.CLEAR_ALL_BUTTON_DONE);
      // for (var i = 0; i < document.querySelectorAll('.btn_done').length; i++) {
      //   if (!document.querySelectorAll('.btn_done')[i].classList.contains('disabled'))
      //     document.querySelectorAll('.btn_done')[i].classList.add('disabled');
      //   }
    })
    .error(function (err) {
      toast(err.status + ': ' + err.statusText);
    });
});

listen(actions.API_DELETE, (id) => {
  $.ajax({
    method: 'DELETE',
    url: ko + '/api.php/' + bd + '/' + id
  })
    .done(function (msg) {
      if (msg) {
        //  console.log(msg);
        emit(actions.API_GET_ALL);
        toast('DELETED =(');
      }
    })
    .error(function (err) {
      //  console.log(err);
      toast(err.status + ': ' + err.statusText);
    });
});

listen(actions.API_PUT_CHANGE_ITEM, (obj) => {
  $.ajax({
    method: 'PUT',
    url: ko + '/api.php/' + bd + '/' + obj.id,
    data: obj
  })
    .done(function (msg) {
      if (msg) {
        //  console.log(msg);
        emit(actions.API_GET_ALL);
        toast('EDITED !');
      }
    })
    .error(function (err) {
      toast(err.status + ': ' + err.statusText);
    });
});

listen(actions.API_ADD_NEW_ITEM, (obj) => {
  $.ajax({
    method: 'POST',
    url: ko + '/api.php/' + bd + '/',
    data: obj
  })
    .done(function (msg) {
      if (msg) {
        //  console.log(msg);
      }
      emit(actions.API_GET_ALL);
      toast('ADDED NEW, hi ');
    })
    .error(function (err) {
      toast(err.status + ': ' + err.statusText);
    });
});
