/*  jshint esversion: 6 */
/*  global Materialize*/

function toast (data) {
  Materialize.toast(data, 6000);
}

export default toast;
