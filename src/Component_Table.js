/**
 * Created by vitalii on 19.02.16.
 */
/*  jshint esversion: 6 */

import React from 'react';
//  import { render } from 'react-dom'
import { emit } from './dispatcher';
import actions from './actions';
import { getState, addChangeListener } from './store';

const Table = React.createClass({
  getInitialState () {
    return getState();
  },
  componentDidMount () {
    emit(actions.API_GET_ALL);
    addChangeListener(this._update);
  },
  _update () {
    this.setState(getState());
  },
  _change (i, property, event) {
  //  event.target.parentNode.querySelector('a').classList.remove('disabled');
    this.state.data[i][property] = event.target.innerText;
    emit(actions.CHANGE_TABLE_STATE, this.state.data);
    emit(actions.ADD_BUTTON_DONE, this.state.data[i].id);
  },
  _remove (i) {
    var name = '';
    Object.keys(this.state.data[i]).map((value, index) => {
      if (index === 2) {
        name = value;
      }
    });
    if (confirm('Do you want remove ' + this.state.data[i][name])) {
      emit(actions.API_DELETE, this.state.data[i].id);
    }
  },
  _done (i, event) {
    emit(actions.STORE_PUT_CHANGE_ITEM, this.state.data[i].id);
  },
  _sort (item, i) {
    emit(actions.SORT, item.toLowerCase());
  },
  render () {
    return (
      <table className='striped'>
        <thead>
          <tr>
          {
            this.state.colums.map((item, i) =>
              <th key={i} onClick={this._sort.bind(this, item, i)}>{item}</th>
            )
          }
          </tr>
        </thead>
        <tbody>

        {
          this.state.data.map((item, i) =>
          <tr key={i}>
          {
            Object.keys(item).map((value, index) =>
              <td key={index} contentEditable={true} onInput={this._change.bind(this, i, value)}>{item[value]}</td>
            )
          }
          {
            this.state.buttonDoneToShow.map(function (pos, ind) {
              //  console.log(pos, this.state.data[i].id);
              if (pos === this.state.data[i].id) {
                return <td key={ind}><a className='waves-effect btn btn_done' onClick={this._done.bind(this, i)} ><i className='material-icons' >done</i></a></td>;
              } else {
                return;
              }
            }.bind(this))
          }
          {/* <td><a className='waves-effect btn disabled btn_done' onClick={this._done.bind(this, i)} ><i className='material-icons' >done</i></a></td>*/}
          <td><i className='material-icons remove' onClick={this._remove.bind(this, i)} >delete_forever</i></td>
          </tr>
          )
        }

        </tbody>
      </table>
    );
  }
});

export default Table;
