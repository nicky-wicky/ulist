/*  jshint esversion: 6 */

import { listen, emit } from './dispatcher';
import actions from './actions';
import './api';

const state = {
  data: [],
  search: '',
  bak: [],
  colums: [],
  bool: 2,
  buttonDoneToShow: []
};

const listeners = [];

export function getState () {
  return state;
}

export function addChangeListener (fn) {
  listeners.push(fn);
}

function notify () {
  listeners.forEach((fn) => fn());
}

listen(actions.ADD_BUTTON_DONE, (id) => {
  var check = false;

  for (var i = 0; i < state.buttonDoneToShow.length; i++) {
    if (state.buttonDoneToShow[i] === id) {
      check = true;
    }
  }

  if (!check) {
    state.buttonDoneToShow.push(id);
  }

  notify();
});

listen(actions.CLEAR_ALL_BUTTON_DONE, () => {
  state.buttonDoneToShow = [];
  notify();
});

listen(actions.CHANGE_TABLE_STATE, (data) => {
  state.data = data;
  notify();
});

listen(actions.STORE_ADD_NEW_FIELD, () => {
  let arr = [];
  for (var i = 0; i < state.data.length; i++) {
    arr.push(state.data[i].id);
  }

  var maxId = Math.max.apply({}, arr);
  var obj = {
    id: maxId + 1,
    name: '-',
    ip: '-',
    description: '-'
  };

  emit(actions.API_ADD_NEW_ITEM, obj);
});

listen(actions.SEARCH, (data) => {
  function sr (data) {
    //  console.log('пришло искать: ', data);
    state.search = data;
    var newStateAfterSearch = [];
    var regexp = new RegExp(data, 'gim');
    for (var key in state.data) {
      for (var key2 in state.data[key]) {
        if (state.data[key][key2].search(regexp) !== -1) {
          //  console.log('нашло: ', state.data[key]);
          // if(newStateAfterSearch.length > 0) {
          for (var i = 0; i < newStateAfterSearch.length; i++) {
              //  console.log('длина: ', newStateAfterSearch.length);
            var check = false;
            if (JSON.stringify(newStateAfterSearch[i]) === JSON.stringify(state.data[key])) {
              check = true;
            }
          }
          if (!check) {
            newStateAfterSearch.push(state.data[key]);
          }
        }
      }
    }
    state.data = newStateAfterSearch;

    notify();
  }

  if (data === '') {
    emit(actions.API_GET_ALL);
  }

  if (state.search.length > data.length) {
    state.data = state.bak;
    sr(data);
  } else {
    sr(data);
  }
});

listen(actions.SORT, (data) => {
  if (state.bool % 2 === 0) {
    state.data.sort(function (a, b) {
      // if(a[data] < b[data]) return -1;
      // if(a[data] > b[data]) return 1;
      //
      // return 0;
      return a[data] - b[data];
    });
    state.bool++;
  } else {
    state.data.sort(function (a, b) {
      // if(a[data] < b[data]) return 1;
      // if(a[data] > b[data]) return -1;
      //
      // return 0;
      return b[data] - a[data];
    });
    state.bool++;
  }
  notify();
});

listen(actions.API_GET_ALL_SUCCESS, (data) => {
  data.sort(function (a, b) {
    return b.id - a.id;
  });

  state.data = data;
  state.bak = state.data;
  var colums = [];

  for (var key in state.data[0]) {
    colums.push(key.toUpperCase());
  }
  state.colums = colums;

  notify();
});

listen(actions.STORE_PUT_CHANGE_ITEM, (id) => {
  for (var key in state.data) {
    if (state.data[key].id === id) {
      emit(actions.API_PUT_CHANGE_ITEM, state.data[key]);
    }
  }
});
