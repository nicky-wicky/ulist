/**
 * Created by vitalii on 26.02.16.
 */
 /* jshint esversion: 6 */

import React from 'react';
//  import { render } from 'react-dom'
import { emit } from './dispatcher';
import actions from './actions';
//  import { getState, addChangeListener } from './store'
import toast from './toast';

const Navbar = React.createClass({
  // getInitialState() {
  //
  // return getState();
  // },
  // componentDidMount() {
  //
  // addChangeListener(this._update);
  // },
  // _update() {
  //
  // this.setState(getState());
  // },
  _handleClick () {
    emit(actions.STORE_ADD_NEW_FIELD);
  },
  _handleClick_apply () {
    toast('Everything is good ;)');
  },
  search (event) {
    emit(actions.SEARCH, event.target.value);
  },
  render () {
    return (
      <nav>
        <div className='nav-wrapper blue darken-1'>
          <a href='#' className='brand-logo left'>U List</a>
          <ul className='right'>
          <li>
                 <form>
                   <div className='input-field'>
                     <input className='sr' type='search' required='' onChange={this.search} />
                     <label><i className='material-icons'>search</i></label><i className='material-icons'>close</i>
                   </div>
                 </form>
               </li>
            <li><a href='#' onClick={this._handleClick} ><i className='material-icons left'>add</i>ADD</a></li>
          </ul>
        </div>
      </nav>
    );
  }
});

export default Navbar;
