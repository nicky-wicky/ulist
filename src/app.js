/**
 * Created by vitalii on 26.02.16.
 */
 // https://github.com/mevdschee/php-crud-api
 /* jshint esversion: 6 */
 /* eslint no-console: 2*/

import React from 'react';
import ReactDOM from 'react-dom';
import Table from './Component_Table';
import Navbar from './Component_Navbar.js';
import '../css/style.css';
// import '../css/icons.css';
// import '../materialize/css/materialize.min.css';

ReactDOM.render(<Table />, document.querySelector('#list'));
ReactDOM.render(<Navbar />, document.querySelector('.navbar-fixed'));

window.addEventListener('load', function whenLoad () {
  console.log('page load');
});
