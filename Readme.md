#U List

#About

This is project for IT departments.

It is editable table with list of data.

You can add, edit, delete , search and sort information.

#Technology

Database: MySQL

Front-end: React, Flux, Webpack, ES6, jQuery, AJAX

Style: Materialize.css

API: https://github.com/mevdschee/php-crud-api

#How to start

You must have apache2+php, MySQL Db

Install devDependencies. Go to the folder 'ulist', open terminal and type


```
#!bash

npm i
```

Configure api.php

Configure /src/api.js


```
#!bash

NODE_ENV=production webpack
```
